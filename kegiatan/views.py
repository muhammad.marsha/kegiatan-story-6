from django.shortcuts import render, redirect

# Create your views here.
from .forms import FormKegiatan, FormOrang
from .models import Kegiatan, Orang
from datetime import datetime


def index(request):
    return render(request,'index.html')

def addActivity(request):
    form_kegiatan = FormKegiatan()
    if request.method == "POST":
        form_kegiatan_input = FormKegiatan(request.POST)
        if form_kegiatan_input.is_valid():
            data = form_kegiatan_input.cleaned_data
            kegiatan_input = Kegiatan()
            kegiatan_input.nama_kegiatan = data['nama_kegiatan']
            kegiatan_input.deskripsi_kegiatan = data['deskripsi_kegiatan']
            kegiatan_input.save()
            return redirect('/')
        else:
            return render(request, 'addactivity.html', {'form': form_kegiatan, 'status': 'failed'})
    else:
        return render(request, 'addactivity.html', {'form': form_kegiatan})


def listActivity(request):
    kegiatan = Kegiatan.objects.all()
    orang = Orang.objects.all()
    return render(request, 'listActivity.html', {'kegiatan': kegiatan, 'orang': orang})


def deleteUser(request, delete_id):
    kegiatan = Kegiatan.objects.all()
    orang = Orang.objects.all()
    print(delete_id)
    orang_to_delete = Orang.objects.get(id=delete_id)
    orang_to_delete.delete()
    return redirect('/list')


def register(request, task_id):
    form_orang = FormOrang()

    if request.method == "POST":
        form_orang_input = FormOrang(request.POST)
        if form_orang_input.is_valid():
            data = form_orang_input.cleaned_data
            orangBaru = Orang()
            orangBaru.nama_orang = data['nama_orang']
            orangBaru.kegiatan = Kegiatan.objects.get(id=task_id)
            orangBaru.save()
            return redirect('/list')
        else:
            return render(request, 'register.html', {'form': form_orang, 'status': 'failed'})
    else:
        return render(request, 'register.html', {'form': form_orang})
