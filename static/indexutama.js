// var x;
// export { x };
let formInput = document.querySelector('.inputan input');
console.log(formInput);
let x;
formInput.addEventListener('change', function () {
  x = formInput.value;
  //   console.log(x);
  localStorage.setItem('nama', x);
  inputName(formInput.value);
});

function inputName(name) {
  window.location = '/landing';
  // document.getElementsByClassName('inputan')[0].remove();

  // document.getElementsByClassName('containerInputName')[0].appendChild(x);
}

function smoothScroll(target, duration) {
  let target2 = document.querySelector(target);
  let targetPosition = target2.getBoundingClientRect().top;
  // console.log(targetPosition);
  let startPosition = window.pageYOffset;
  let distance = targetPosition - startPosition;
  let startTime = null;

  function animation(currentTime) {
    if (startTime == null) {
      startTime = currentTime;
    }
    let timElapsed = currentTime - startTime;
    let run = ease(timElapsed, startPosition, distance, duration);
    window.scrollTo(0, run);
    if (timElapsed < duration) requestAnimationFrame(animation);
  }

  function ease(t, b, c, d) {
    t /= d / 2;
    if (t < 1) return (c / 2) * t * t + b;
    t--;
    return (-c / 2) * (t * (t - 2) - 1) + b;
  }

  requestAnimationFrame(animation);
}

let section1 = document.querySelector('.containerAtas');
section1.addEventListener('click', function () {
  smoothScroll('.containerInputName', 2000);
});
